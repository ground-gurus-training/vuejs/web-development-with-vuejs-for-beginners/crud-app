import '@/firebase';

import { createApp } from 'vue';
import App from './App.vue';

import router from './router';
import store from './store';

import PrimeVue from 'primevue/config';
import Button from 'primevue/button';
import InputText from 'primevue/inputtext';
import Message from 'primevue/message';
import Textarea from 'primevue/textarea';
import Dialog from 'primevue/dialog';
import ConfirmService from 'primevue/confirmationservice';
import ConfirmDialog from 'primevue/confirmdialog';

const app = createApp(App);

app.use(PrimeVue, { ripple: true });
app.use(ConfirmService);
app.use(router);
app.use(store);

app.component('Button', Button);
app.component('InputText', InputText);
app.component('Message', Message);
app.component('Textarea', Textarea);
app.component('Dialog', Dialog);
app.component('ConfirmDialog', ConfirmDialog);

app.mount('#app');

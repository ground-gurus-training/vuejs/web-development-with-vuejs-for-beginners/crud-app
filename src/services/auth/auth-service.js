import { getAuth, signInWithEmailAndPassword, signOut, createUserWithEmailAndPassword, sendPasswordResetEmail } from 'firebase/auth';

const auth = getAuth();

class AuthService {
    getCurrentUser() {
        return auth.currentUser;
    }

    login(email, password) {
        return signInWithEmailAndPassword(auth, email, password);
    }

    logout() {
        signOut(auth);
    }

    register(email, password) {
        return createUserWithEmailAndPassword(auth, email, password);
    }

    sendForgotPassword(email) {
        return sendPasswordResetEmail(auth, email);
    }
}

const authService = new AuthService();

export default authService;

import { getFirestore, collection, doc, getDoc, getDocs, addDoc, deleteDoc } from 'firebase/firestore';

const db = getFirestore();

class NoteService {
    async findAll() {
        return await getDocs(collection(db, 'Notes'));
    }

    async create(note) {
        return await addDoc(collection(db, 'Notes'), note);
    }

    async delete(id) {
        const docRef = doc(db, "Notes", id);
        const docSnap = await getDoc(docRef);
        if (docSnap.exists()) {
            deleteDoc(docRef);
            return true;
        } else {
            console.error(`No such document with id $id`);
            return false;
        }
    }
}

const noteService = new NoteService();

export default noteService;

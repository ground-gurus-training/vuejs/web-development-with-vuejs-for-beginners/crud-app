import { initializeApp } from "firebase/app";

const firebaseConfig = {
  apiKey: "AIzaSyBSxlYYils_VlN1hJFNBbhdaZN01TWT12k",
  authDomain: "vuejs-gg-training.firebaseapp.com",
  projectId: "vuejs-gg-training",
  storageBucket: "vuejs-gg-training.appspot.com",
  messagingSenderId: "69742747611",
  appId: "1:69742747611:web:cdbe04dc7d553f7ea5d1fe"
};

const app = initializeApp(firebaseConfig);

export default app;

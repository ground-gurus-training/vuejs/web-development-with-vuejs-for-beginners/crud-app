import { createStore } from "vuex";

import noteService from '@/services/note-service';

export default createStore({
    state: {
        notes: []
    },
    mutations: {
        async init(state) {
            state.notes = [];

            const notesSnapshot = await noteService.findAll();
            notesSnapshot.forEach((doc) => {
                const { title, note } = doc.data();
                state.notes.push({
                    id: doc.id,
                    title,
                    note,
                });
            });
        }
    }
});

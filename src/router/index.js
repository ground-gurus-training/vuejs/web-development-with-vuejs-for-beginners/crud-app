import { createRouter, createWebHistory } from 'vue-router';

const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('@/views/Home')
  },
  {
    path: '/login',
    name: 'Login',
    component: () => import('@/views/auth/Login')
  },
  {
    path: '/register',
    name: 'Register',
    component: () => import('@/views/auth/Register')
  },
  {
    path: '/forgotPassword',
    name: 'ForgotPassword',
    component: () => import('@/views/auth/ForgotPassword')
  }
];

const router = createRouter({
  history: createWebHistory(),
  routes
});

export default router;

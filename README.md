# crud-app

## Project setup

```sh
yarn install
```

### Compiles and hot-reloads for development

```sh
yarn serve
```

### Compiles and minifies for production

```sh
yarn build
```

### Lints and fixes files

```sh
yarn lint
```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).

## Deploying to the Cloud

### Login to Firebase

```sh
firebase login
```

### Build the project

```sh
yarn build
```

### Deploy it to Firebase Hosting

```sh
yarn deploy
```
